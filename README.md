[![Deploy to IBM Cloud](https://bluemix.net/deploy/button.png)](https://bluemix.net/deploy?repository=https%3A//gitlab.com/rakket/fh.git)

## Getting Started

Run `npm i && npm start` to work on the project.

Run `npm i && npm run build` for a production build.
