import React, { Component } from 'react';
import Helmet from 'react-helmet'
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';

// Components
import Header from './components/Header/Header.jsx';
import Footer from './components/Footer/Footer.jsx';

// Pages
import LoginPage from './pages/LoginPage.jsx';
import WizardPage from './pages/WizardPage.jsx';

import './App.css';

class App extends Component {
  render() {
    return (
      <Router>
        <div className='App'>
          <Helmet
            title='Fleetwood Hack!'
            meta={[
              { name: 'description', content: 'Sample' },
              { name: 'keywords', content: 'sample, something' },
            ]}
          />

          <Header/>

          <main className='App-main'>
            <Switch>
              <Route
                exact
                path='/'
                render = { () => {
                  return <LoginPage { ...this.props } />
                }}
              />

              <Route
                exact
                path='/wizard'
                render = { () => {
                  return <WizardPage { ...this.props } />
                }}
              />
            </Switch>
          </main>

          <Footer/>
        </div>
      </Router>
    );
  }
}

export default App;
