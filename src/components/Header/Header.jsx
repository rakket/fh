import React from 'react';

import './Header.css';

export default class Header extends React.Component {
  render() {
    return (
      <header className='Header'>
        <div className='Header-wrapper'>
          <span className='Header-brand'>myBanker</span>
        </div>
      </header>
    )
  }
}