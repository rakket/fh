import React from 'react';

import Card from '../Card/Card.jsx';
import FormGroup from '../FormGroup/FormGroup.jsx';
import ButtonWrapper from '../ButtonWrapper/ButtonWrapper.jsx';
import Button from '../Button/Button.jsx';

export default class Login extends React.Component {
  render() {
    return ( 
      <Card
        headingContent='Welcome!'
        headingLevel='1'
      >
        <FormGroup
          id='username'
          type='text'
          label='Username'
        />

        <FormGroup
          id='password'
          type='password'
          label='Password'
        />

        <ButtonWrapper>
          <Button
            type='primary'
            content='Log In'
            fullWidth={ true }
          />
        </ButtonWrapper>
      </Card>
    )
  }
}