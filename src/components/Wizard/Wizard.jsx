import React from 'react';
import { NavLink } from 'react-router-dom';
import { Icon } from 'react-icons-kit';
import { mail } from 'react-icons-kit/feather/mail';
import { smartphone } from 'react-icons-kit/feather/smartphone'
import { users } from 'react-icons-kit/feather/users'

import Heading from '../Heading/Heading.jsx';
import WizardAnswer from './WizardAnswer.jsx';

import './Wizard.css';

export default class Wizard extends React.Component {
  constructor( props ) {
    super( props );

    this.state = {
      question: 'How can we help?',
      answers: [
        {
          id: 'answer-1',
          content: 'Do you have a question about an existing account?'
        },
        {
          id: 'answer-2',
          content: 'Do you have a question about a new service?'
        }
      ]
    }
  }
  render() {
    const {
      question,
      answers
    } = this.state;

    return (
      <div className='Wizard'>
        <Heading
          content={ question }
          className='Wizard-heading'
          level='1'
        />

        { Object.keys( answers ).map( answer => {
            return (
              <WizardAnswer
                id={ answers[answer].id }
                key={ answers[answer].id }
                content={ answers[answer].content }
              />
            )
          })
        }

        <div className='Wizard-help'>
          <p>Need help now? Reach out.</p>

          <div className='Wizard-links'>
            <a className='Wizard-link' href='mailto:marketleader@bbandt.com'>
              <Icon icon={ mail } className='Wizard-linkIcon'/>

              <span className='Wizard-linkDescription'>By Email</span>
            </a>

            <NavLink className='Wizard-link' to='/'>
              <Icon icon={ smartphone } className='Wizard-linkIcon'/>

              <span className='Wizard-linkDescription'>By Phone</span>
            </NavLink>

            <NavLink className='Wizard-link' to='/schedule'>
              <Icon icon={ users } className='Wizard-linkIcon'/>

              <span className='Wizard-linkDescription'>Or In-Person</span>
            </NavLink>
          </div>
        </div>
      </div>
    )
  }
}