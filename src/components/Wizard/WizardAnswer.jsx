import React from 'react';

import './WizardAnswer.css';

export default class WizardAnswer extends React.Component {
  render() {
    const {
      to,
      content,
      id
    } = this.props;

    return (
      <button className='WizardAnswer' id={ id } to={ to } { ...this.props }>
        { content }
      </button>
    )
  }
}