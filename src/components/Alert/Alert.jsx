import React from 'react';
import classnames from 'classnames';
import { Icon } from 'react-icons-kit';
import { checkCircle } from 'react-icons-kit/feather/checkCircle';
import { alertCircle } from 'react-icons-kit/feather/alertCircle';
import { alertTriangle } from 'react-icons-kit/feather/alertTriangle';
import { info } from 'react-icons-kit/feather/info';
import { x } from 'react-icons-kit/feather/x'

import './Alert.css'

export default class Alert extends React.Component {
  constructor( props ) {
    super( props );

    this.alertContent = React.createRef();

    this.state = {
      isHidden: false
    }

    this.show = this.show.bind( this );
    this.hide = this.hide.bind( this );
  }

  componentDidMount() {
    this.alertContent.current.focus()
  }

  show() {
    this.setState( {
      isHidden: false
    } )
  }

  hide() {
    this.setState( {
      isHidden: true
    } )
  }

  render() {
    const {
      type,
      content,
      classes,
      isDismissable,
      closeOnClick
    } = this.props;
    
    const calculatedClassNames = `Alert Alert--${type} ${ classnames( { classes } ) }`
    const isHidden = this.state.isHidden

    return (
      <React.Fragment>
        { !isHidden &&
          <div 
            className={ calculatedClassNames } 
            role='alert' 
            tabIndex='-1'
          >
            <div className='Alert-pre'>
              { type === 'error' &&
                <Icon icon={ alertCircle } className='Alert-icon' />
              }

              { type === 'success' &&
                <Icon icon={ checkCircle } className='Alert-icon' />
              }

              { type === 'attention' &&
                <Icon icon={ alertTriangle } className='Alert-icon' />
                
              }

              { type === 'info' &&
                <Icon icon={ info } className='Alert-icon' />
              }
            </div>

            <div className='Alert-content' ref={ this.alertContent } tabIndex='-1'>
              { content }
            </div>

            { isDismissable &&
              <button
                className='Alert-close'
                onClick={ closeOnClick }
                aria-label='Close'
              >
                <Icon icon={ x }/>
              </button>
            }
          </div>
        }
     </React.Fragment>
    )
  }
}