import React from 'react';

import './Heading.css';

export default class Heading extends React.Component {
  render() {
    const {
      level,
      content,
      alignment,
      className
    } = this.props;
    
    const classes = `Heading Heading--${ level } ${ className }`

    return (
      <div 
        className={ classes }
        role='heading'
        aria-level={ level }
        style={{ textAlign: alignment }}
      >
        { content }
      </div>
    )
  }
}