import React from 'react';

import './DarkLayout.css';
import backgroundImage from './office.png';

export default class DarkLayout extends React.Component {
  render() {
    return (
      <div className='DarkLayout'>
        <span className='DarkLayout-angledThingy'>
          <img 
            className='DarkLayout-backgroundImage' 
            src={ backgroundImage } 
            alt='' 
            aria-hidden='true'
          />
        </span>

        <div className='DarkLayout-wrapper'>
          

          { this.props.children }
        </div>
      </div>
    )
  }
}