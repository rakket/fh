import React from 'react';

import './DefaultLayout.css';

export default class DefaultLayout extends React.Component {
  render() {
    return (
      <div className='DefaultLayout'>
        <div className='DefaultLayout-wrapper'>
          { this.props.children }
        </div>
      </div>
    )
  }
}