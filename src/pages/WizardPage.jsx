import React from 'react';

import DarkLayout from '../layouts/DarkLayout.jsx'

import Wizard from '../components/Wizard/Wizard.jsx';

export default class WizardPage extends React.Component {
  render() {
    return (
      <DarkLayout>
        <Wizard/>
      </DarkLayout>
    )
  }
}