import React from 'react';

import DefaultLayout from '../layouts/DefaultLayout.jsx';

import Login from '../components/Login/Login.jsx';

export default class LoginPage extends React.Component {
  render() {
    return (
      <DefaultLayout>
        <Login/>
      </DefaultLayout>
    )
  }
}